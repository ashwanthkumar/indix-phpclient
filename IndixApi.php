<?php

define("API_VERSION","v1");
define("API_BASE_URL", "http://api.indix.com/api/" . API_VERSION);
define("INDIX_SEARCH_STORES", "/search/stores/");
define("INDIX_SEARCH_BRANDS", "/search/brands/");
define("INDIX_SEARCH_PRODUCTS", "/search/productSearch/");
define("INDIX_PRODUCT_DETAILS", "/products/details/");
define("INDIX_PRICE_HISTORY", "/products/price_history/");
define("INDIX_EXPORT_CATEGORIES", "/export/categories/");

define("SORT_BY_RELEVANCE", "RELEVANCE");
define("SORT_BY_PRICE_LOW_TO_HIGH", "PRICE_LOW_TO_HIGH");
define("SORT_BY_PRICE_HIGH_TO_LOW", "PRICE_HIGH_TO_LOW");
define("SORT_BY_MOST_RECENT", "MOST_RECENT");

define("DEBUG", FALSE);

/*
	PHP5 Client for Indix.com API

	- Full documentation on the response attributes can be found on 
	  https://developer.indix.com/docs
	- You need valid ApplicationId (app_id) and ApplicationKey (app_key) from
	  https://developer.indix.com/
*/
class IndixApi {
	private $app_id;
	private $app_key;

	public function __construct($app_id, $app_key) {
		$this->app_id = $app_id;
		$this->app_key = $app_key;
	}

	public function productDetails($productId, $page_number = 1) {
		return $this->validate_data(
			$this->makeRequest(INDIX_PRODUCT_DETAILS, 
				array("productId" => $productId, "page_number" => $page_number)));
	}

	public function getPriceHistory($productId) {
		return $this->validate_data(
			$this->makeRequest(PRICE_HISTORY, 
				array("productId" => $productId)));
	}

	public function searchStores($query) {
		return $this->validate_data($this->makeRequest(INDIX_SEARCH_STORES, array("query" => $query)));
	}

	public function searchBrands($query) {
		return $this->validate_data($this->makeRequest(INDIX_SEARCH_BRANDS, array("query" => $query)));
	}

	public function searchProducts(	$query, $store_id = NULL, $brand_id = NULL, 
									$category_id = NULL, $start_price = 0, 
									$end_price = PHP_INT_MAX, $sort_by = SORT_BY_RELEVANCE, 
									$page_number = 1) {
		$params = array("query" => $query);

		if(!is_null($store_id)) $params['store_id'] = $store_id;
		if(!is_null($brand_id)) $params['brand_id'] = $brand_id;
		if(!is_null($category_id)) $params['category_id'] = $category_id;
		if(!is_null($start_price)) $params['start_price'] = $start_price;
		if(!is_null($end_price)) $params['end_price'] = $end_price;
		if(!is_null($sort_by)) $params['sort_by'] = $sort_by;
		if(!is_null($page_number)) $params['page_number'] = $page_number;

		return $this->validate_data($this->makeRequest(INDIX_SEARCH_PRODUCTS, $params));
	}

	public function exportCategories() {
		$response = $this->makeRequest(INDIX_EXPORT_CATEGORIES);
		$categories = array();

		$response_as_lines = explode("\n", $response);
		foreach ($response_as_lines as $category_info_in_csv) {
			$category_info = str_getcsv($category_info_in_csv);
			if(count($category_info) == 3) {
				$category['id'] = $category_info[0];
				$category['name'] = $category_info[1];
				$category['category_path'] = $category_info[2];
	
				$categories[] = $category;
			}
		}

		debug("Categories Found: " . count($categories));
		return $categories;
	}

	private function makeRequest($end_point, $params = array()) {
	    if(!function_exists('curl_init')) throw new IndixException('cURL is not installed on your server');

	    $params['app_key'] = $this->app_key;
	    $params['app_id'] = $this->app_id;
	    $request_url = API_BASE_URL . $end_point . "?" . http_build_query($params);

	    debug("Request URL: " . $request_url);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $request_url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        $response_from_server = curl_exec($curl);
        $this->check_for_internal_error($request_url, $response_from_server);
        return $response_from_server;
	}

	private function validate_data($response) {
		$parsed_response = json_decode($response, FALSE);
		if($parsed_response->status != 200) throw new IndixException("API Error: " . $parsed_response->message);
		
		return $parsed_response->data;
	}

	private function check_for_internal_error($request_url, $response) {
		if(stristr($response, "No rule matched")) {
			throw new IndixException("Invalid API end point " . $request_url);
		} elseif (stristr($response, "Authentication failure")) {
			throw new IndixException("Invalid API Id (" . $this->app_id . ") / API Key (" . $this->app_key . ")");
		}
	}
}

class IndixException extends Exception {}

class IndixExtendedApi extends IndixApi {

	public function searchProductsInStore($query, $store_id, $page_number = 1) {
		return $this->searchProducts($query, $store_id, NULL, NULL, 0, PHP_INT_MAX, SORT_BY_RELEVANCE, $page_number);
	}

	public function searchProductsInPriceRange($query, $start_price = 0, $end_price = PHP_INT_MAX, $page_number) {
		return $this->searchProducts($query, NULL, NULL, NULL, $start_price, $end_price, SORT_BY_RELEVANCE, $page_number);
	}

	public function searchProductsInBrand($query, $brand_id, $page_number = 1) {
		return $this->searchProducts($query, NULL, $brand_id, NULL, 0, PHP_INT_MAX, SORT_BY_RELEVANCE, $page_number);
	}

	public function searchProductsInStoreAndBrand($query, $store_id, $brand_id, $page_number = 1) {
		return $this->searchProducts($query, $store_id, $brand_id, NULL, 0, PHP_INT_MAX, SORT_BY_RELEVANCE, $page_number);
	}

	public function searchProductsFromCategory($query, $category_id, $page_number = 1) {
		return $this->searchProducts($query, NULL, NULL, $category_id, 0, PHP_INT_MAX, SORT_BY_RELEVANCE, $page_number);
	}

	public function searchProductsByUpc($upc, $page_number = 1) {
		return $this->searchProducts("upc:" . $upc, NULL, NULL, NULL, 0, PHP_INT_MAX, SORT_BY_RELEVANCE, $page_number);
	}

	public function searchProductsBySku($sku, $page_number = 1) {
		return $this->searchProducts("sku:" . $sku, NULL, NULL, NULL, 0, PHP_INT_MAX, SORT_BY_RELEVANCE, $page_number);
	}

	public function searchProductsByMpn($sku, $page_number = 1) {
		return $this->searchProducts("mpn:" . $sku, NULL, NULL, NULL, 0, PHP_INT_MAX, SORT_BY_RELEVANCE, $page_number);
	}

}

function debug($message) {
	if(DEBUG) {
		echo "[DEBUG] " . date("r") . " => " . $message . "\n";
	}
}
