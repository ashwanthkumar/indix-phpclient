# Indix API (PHP5 Client)

PHP5 Client to [Indix API](https://developer.indix.com/ "Indix Developer Portal"). 

### Normal Usage

```
require_once("IndixApi.php");

$api = new IndixApi("__MY_APP_ID__","__MY_APP_KEY__");

// Get list of all categories
$api->exportCategories();

// Search brands by name
$api->searchBrands("adidas");

// Search stores by name
$api->searchStores("walgreens");

// Search products by name
$api->searchProducts("nike air max");

// Get product Details using a product Id from searchProducts
$api->productDetails("ee3f8d4bfd63bf0f3a4fe3df7c30ed95");

// Get Price History for a product
$api->getPriceHistory("ee3f8d4bfd63bf0f3a4fe3df7c30ed95");

```

### Extended API Usage

We have an Extended Client that abstracts commonly used queries, such as search by UPC, MPN / SKU.

```
$extendedApi = new IndixExtendedApi("__MY_APP_ID__","__MY_APP_KEY__");

// Search by UPC, SKU or MPN
$extendedApi->searchProductsByUpc("7585601147");
$extendedApi->searchProductsByMpn("487789-008");
$extendedApi->searchProductsBySku("B005OC0YX2");

```
